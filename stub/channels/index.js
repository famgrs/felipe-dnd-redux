const mock = require('./mock')

const show = {
  method: 'GET',
  path: '/channel/list',
  handler: (req, reply) => {
    reply(mock.channels)
  }
}

const save = {
  method: 'GET',
  path: '/channel/create',
  handler: (req, reply) => reply(mock.saveChannel)
}

module.exports = {
  show,
  save
}
