const channels = {
  rows: [{
    id: 0,
    name: 'Messenger',
    time: new Date()
  }, {
    id: 1,
    name: 'SMS',
    time: new Date()
  }, {
    id: 2,
    name: 'WhatsApp',
    time: new Date()
  }]
}

const saveChannel = {
  rows: {
    id: 4,
    name: 'Telegram',
    time: new Date()
  },
  status: 204,
  message: 'channel created successfully'
}

module.exports = {
  channels,
  saveChannel
}
