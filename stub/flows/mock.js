const flows = {
  rows: [{
    id: 0,
    name: 'Flow 1',
    time: new Date(),
    owner: 'Bot 1',
    sequence: [{
      type: 'trigger',
      subtype: 'intention',
      name: 'Consumption'
    }, {
      type: 'answer',
      subtype: 'image',
      content: '<img src"" />'
    }]
  }, {
    id: 1,
    name: 'Flow 2',
    time: new Date(),
    owner: 'Bot 1',
    sequence: [{
      type: 'trigger',
      subtype: 'intention',
      name: 'Indications'
    }, {
      type: 'answer',
      subtype: 'geo point',
      content: 'https://maps.google.com'
    }]
  }]
}

const saveFlow = {
  rows: {
    id: 1,
    name: 'Flow 3',
    time: new Date(),
    owner: 'Bot 1',
    sequence: [{
      type: 'trigger',
      subtype: 'intention',
      name: 'Wallet'
    }, {
      type: 'answer',
      subtype: 'text',
      content: '$ 99.99'
    }]
  },
  status: 204,
  message: 'flow created successfully'
}

module.exports = {
  flows,
  saveFlow
}
