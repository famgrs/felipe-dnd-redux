const mock = require('./mock')

const show = {
  method: 'GET',
  path: '/flow/list',
  handler: (req, reply) => {
    reply(mock.flows)
  }
}

const save = {
  method: 'GET',
  path: '/flow/create',
  handler: (req, reply) => reply(mock.saveFlow)
}

module.exports = {
  show,
  save
}
