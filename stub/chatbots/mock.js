const chatbots = {
  rows: [{
    id: 1,
    title: 'Bot Jao',
    owner: 'hacker_red',
    time: new Date()
  }, {
    id: 2,
    title: 'Bot Ze',
    owner: 'hacker_black',
    time: new Date()
  }, {
    id: 3,
    title: 'Bot Tõim',
    owner: 'hacker_white',
    time: new Date()
  }]
}

const saveChatbot = {
  rows: {
    id: 4,
    title: 'Bot Mane',
    owner: 'hacker_green',
    time: new Date()
  },
  status: 204,
  message: 'chatbot created successfully'
}

module.exports = {
  chatbots,
  saveChatbot
}
