const mock = require('./mock')

const show = {
  method: 'GET',
  path: '/chatbot/list',
  handler: (req, reply) => {
    reply(mock.chatbots)
  }
}

const save = {
  method: 'GET',
  path: '/chatbot/create',
  handler: (req, reply) => reply(mock.saveChatbot)
}

module.exports = {
  show,
  save
}
