module.exports = {
  shoes: require('./shoes'),
  chatbots: require('./chatbots'),
  channels: require('./channels'),
  flows: require('./flows')
}
