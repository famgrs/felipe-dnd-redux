import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { actionTypes } from '../redux/actions'
import Drag from '../components/drag'
import Drop from '../components/drop'

class Main extends React.Component {
  onDrop() {
    console.log('main onDrop')
  }
  onDragOver() {
    console.log('main onDragOver')
  }

  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
          <Drag type="team_blue" />
          <Drag type="team_blue" />
          <Drag type="team_red" />
          <Drag type="team_red" />
        </div>
        <div>
          <Drop />
          <Drop type="team_blue" onDrop={() => this.onDrop()} onDragOver={() => this.onDragOver()} />
        </div>
      </div>
    )
  }
}

const mapProps = state => ({
  currentDrag: state.teste
})
export default connect(null, { ...actionTypes })(Main)
