import React, { Component } from 'react';
import { createStore } from "redux";
import { Provider } from 'react-redux'
import reducer from './redux/reducer'
import logo from './logo.svg';
import './App.css';
import Main from './main'

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <Main />
        </div>
      </Provider>
    );
  }
}

export default App;
