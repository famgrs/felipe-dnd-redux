import { createReducer } from './redux'
import { actionTypes } from './actions'

export const initialState = {

}

export const reducer = {
  [actionTypes.HIGHLIGHT_GOOD_DROPS](state, data) {
    console.log('HIGHLIGHT_GOOD_DROPS')
    return { ...state }
  },
  [actionTypes.MOVE_DRAG](state, data) {
    console.log('MOVE_DRAG')
    return { ...state }
  },
}

export default createReducer(initialState, reducer)
