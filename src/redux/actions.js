export const actionTypes = {
  HIGHLIGHT_GOOD_DROPS: 'DRAG/HIGHLIGHT_GOOD_DROPS',
  MOVE_DRAG: 'DRAG/MOVE_DRAG'
}

export const actions = {
  highlightGoodDrops: () => ({ type: actionTypes.HIGHLIGHT_GOOD_DROPS }),
  moveDrag: () => ({ type: actionTypes.MOVE_DRAG })
}