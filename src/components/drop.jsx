import React from 'react'
import PropTypes from 'prop-types'
import Drop from './drop.styled'

class Droppable extends React.Component {
  onDragOver(ev) {
    console.log('onDragOver')
    ev.preventDefault() // to allow drop
    // this.props.onDragOver()
  }
  onDrop(ev) {
    console.log('onDrop')
    // this.props.onDrop()
  }
  render() {
    const { acceptedTypes } = this.props
    return (
      <Drop onDragOver={() => this.onDragOver()} onDrop={() => this.onDrop()}>

      </Drop>
    )
  }
}
Droppable.propTypes = {
  type: PropTypes.string,
  onDrop: PropTypes.func,
  onDragOver: PropTypes.func
}
export default Droppable
