import React from 'react'
import PropTypes from 'prop-types'
import Drag from './drag.styled'

// https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API

export class Draggable extends React.Component {
  onStart(ev) {
    const { dataToDrag, type, setImage } = this.props
    this.dt = ev.dataTransfer
    this.dt.setData(type, dataToDrag || ev.target)

    if (setImage) {
      const img = new Image()
      img.src = setImage.src
      this.dt.setDragImage(img, setImage.x || 0, setImage.y || 0)
    }
  }
  render() {
    const { children } = this.props
    return (
      <Drag draggable onDragStart={() => this.onStart()}>
        {children}
      </Drag>
    )
  }
}
Draggable.PropTypes = {
  type: PropTypes.string,
  children: PropTypes.node,
  dataToDrag: PropTypes.any,
  setImage: PropTypes.shape({
    src: PropTypes.string,
    x: PropTypes.number,
    y: PropTypes.number
  })
}

export default Draggable